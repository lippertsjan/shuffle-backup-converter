# Shuffle Backup Converter

Reads a .bak file created by https://github.com/andybryant/shuffle-android/ and converts it to CSV.

## Technicalities

The backup file is a protobuf serialization. The corresponding definition, `shuffle.proto` was taken from https://github.com/andybryant/shuffle-dto, commit 85d068d.

## Supported Targets

```
x86_64-unknown-linux-gnu
x86_64-pc-windows-gnu (shows a command line window in the back)
```

To build for the specific target, install the corresponding target (and all its requirements). To build for Windows on 
Manjaro:
```bash
sudo pacman -S mingw-w64-gcc
rustup target add x86_64-pc-windows-gnu
rustup target add i686-pc-windows-gnu

# build all targets
cargo build --target i686-pc-windows-gnu
cargo build --target x86_64-pc-windows-gnu
cargo build --target x86_64-unknown-linux-gnu
```
