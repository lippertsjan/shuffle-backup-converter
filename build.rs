extern crate prost_build;

fn main() {
  println!("Starting protobuf code generation...");

  let mut config = prost_build::Config::default();
  config.out_dir("src");
  config.compile_protos(&["src/proto/shuffle.proto"], &["src/"]).unwrap();

  println!("protbuf code generation completed!");
}
