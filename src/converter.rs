use crate::proto::{Catalogue, Date};
use std::collections::BTreeMap;
use chrono::NaiveDateTime;

pub fn convert_to_string(catalogue: Catalogue) -> String {
    let context_map = extract_context_map(&catalogue);
    let project_map = extract_project_map(&catalogue);

    let mut output = String::new();

    let sep = ",";
    let mut header = String::new();
    header.push_str("Id");
    header.push_str(sep);
    header.push_str("Description");
    header.push_str(sep);
    header.push_str("Details");
    header.push_str(sep);
    header.push_str("Project");
    header.push_str(sep);
    header.push_str("Created");
    header.push_str(sep);
    header.push_str("Modified");
    header.push_str(sep);
    header.push_str("Start Date");
    header.push_str(sep);
    header.push_str("Due Date");
    header.push_str(sep);
    header.push_str("Timezone");
    header.push_str(sep);
    header.push_str("All Day");
    header.push_str(sep);
    header.push_str("Order");
    header.push_str(sep);
    header.push_str("Complete");
    header.push_str(sep);
    header.push_str("Active");
    header.push_str(sep);
    header.push_str("Deleted");
    header.push_str(sep);

    for k in context_map.keys() {
        header.push_str(context_map.get(k).unwrap());
        header.push_str(sep);
    }

    header.push_str("cal_event_id");
    header.push_str(sep);
    header.push_str("Tracks_id");
    header.push_str(sep);
    header.push_str("gae_entity_id");
    header.push_str(sep);
    header.push_str("change_set");
    header.push_str(sep);
    header.push_str("version");
    header.push_str("\n");

    output.push_str(&header);


    for t in &catalogue.task {
        let mut line = String::new();

        line.push_str(&to_printable_string(&t.id.to_string()));
        line.push_str(sep);
        line.push_str(&to_printable_string(&t.description));
        line.push_str(sep);
        line.push_str(&to_printable_string(&t.details.clone().get_or_insert(String::new())));
        line.push_str(sep);
        line.push_str(&to_printable_project(&t.project_id, &project_map));
        line.push_str(sep);
        line.push_str(&to_printable_date(&t.created));
        line.push_str(sep);
        line.push_str(&to_printable_date(&t.modified));
        line.push_str(sep);
        line.push_str(&to_printable_date(&t.start_date));
        line.push_str(sep);
        line.push_str(&to_printable_date(&t.due_date));
        line.push_str(sep);
        line.push_str(&to_printable_string(t.timezone.clone().get_or_insert(String::new())));
        line.push_str(sep);
        line.push_str(&to_printable_bool(&t.all_day));
        line.push_str(sep);
        line.push_str(&to_printable_i32(&t.order));
        line.push_str(sep);
        line.push_str(&to_printable_bool(&t.complete));
        line.push_str(sep);
        line.push_str(&to_printable_bool(&t.active));
        line.push_str(sep);
        line.push_str(&to_printable_bool(&t.deleted));
        line.push_str(sep);

        for k in context_map.keys() {
            let contains_context= t.context_ids.contains(k);
            line.push_str(&to_printable_bool(&Some(contains_context)));
            line.push_str(sep);
        }

        line.push_str(&to_printable_i64(&t.cal_event_id));
        line.push_str(sep);
        line.push_str(&to_printable_i64(&t.tracks_id));
        line.push_str(sep);
        line.push_str(&to_printable_i64(&t.gae_entity_id));
        line.push_str(sep);
        line.push_str(&to_printable_i64(&t.change_set));
        line.push_str(sep);
        line.push_str(&to_printable_i32(&t.version));
        line.push_str("\n");


        output.push_str(&line);
    }

    output
}

fn extract_context_map(catalogue: &Catalogue) -> BTreeMap<i64, String> {
    let mut map = BTreeMap::new();
    for c in &catalogue.context {
        map.insert(c.id, c.name.clone());
    }
    map
}

fn extract_project_map(catalogue: &Catalogue) -> BTreeMap<i64, String> {
    let mut map = BTreeMap::new();
    for c in &catalogue.project {
        map.insert(c.id, c.name.clone());
    }
    map
}

fn to_printable_date(date: &Option<Date>) -> String {
    let millis = date.clone().get_or_insert(Date::default()).millis;

    let secs = millis / 1000;
    let date_time_after_a_billion_seconds = NaiveDateTime::from_timestamp(secs, 0);

    date_time_after_a_billion_seconds.format("%F %H-%M").to_string()
}

fn to_printable_bool(bool: &Option<bool>) -> String {
    bool.clone().get_or_insert(false).to_string()
}

fn to_printable_i32(i: &Option<i32>) -> String {
    i.clone().get_or_insert(-1).to_string()
}

fn to_printable_i64(i: &Option<i64>) -> String {
    i.clone().get_or_insert(-1).to_string()
}

fn to_printable_project(project_id: &Option<i64>, project_map: &BTreeMap<i64, String>) -> String {
    match project_id {
        Some(id) => project_map.get(id).clone().get_or_insert(&String::new()).to_string(),
        None => String::new(),
    }
}
fn to_printable_string(s: &String) -> String {
    let doublequote = "\"";
    let double_doublequote = "\"\"";

    let mut ps = String::new();
    ps.push_str(doublequote);
    ps.push_str(s.replace(doublequote, double_doublequote).as_str());
    ps.push_str(doublequote);
    ps
}