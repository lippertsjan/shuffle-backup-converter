//! Crate to convert Shuffle backup files into CSV files.

mod proto;
mod converter;
mod reader;
mod user_interface;

use crate::reader::read_backup_file;
use crate::converter::convert_to_string;

use std::error::Error;
use std::fs;

pub fn use_user_interface() -> Result<(), Box<dyn Error>> {
    user_interface::use_user_interface()
}

/// Reads the given .bak file and converts it to CSV. If <c>args.outputfile</c> is <c>None</c>, the
/// result is printed on STDOUT. Otherwise the CSV is written to the given file.
///
/// # Examples
///
/// Converts `example.bak` into `example.csv`:
/// ```
/// use std::error::Error;
/// use shuffle_backup_converter::{convert_backup_file, Args};
/// fn main() -> Result<(),Box< dyn Error>> {
///     convert_backup_file(&String::from("example.bak"), &String::from("example.csv"))
/// }
/// ```
pub fn convert_backup_file(input_file: &String, output_file: &String) -> Result<(), Box<dyn Error>> {
    let catalogue_result = read_backup_file(&input_file);

    match catalogue_result {
        Ok(catalogue) => {
            let output = convert_to_string(catalogue);
            fs::write(output_file, output)?;
        }
        Err(e) => return Err(Box::new(e))
    }
    Ok(())
}

/// Struct to represent the possible command line arguments.
pub struct Args {
    /// Shuffle .bak file
    pub inputfile: Option<String>,

    /// Optional output file. If <c>None</c> is given, the conversion result is printed to stdout
    pub outputfile: Option<String>,
}

impl Args {
    pub fn default() -> Args {
        Args { inputfile: None, outputfile: None }
    }
}

/// Represents an error when parsing the command line arguments.
#[derive(Debug)]
pub struct CliArgsParsingError {
    details: String
}

impl CliArgsParsingError {
    pub fn new(msg: &str) -> CliArgsParsingError {
        CliArgsParsingError { details: msg.to_string() }
    }
}

impl std::fmt::Display for CliArgsParsingError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.details)
    }
}

impl Error for CliArgsParsingError {
    fn description(&self) -> &str {
        &self.details
    }
}
