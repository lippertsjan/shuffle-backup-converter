use std::env;
use std::error::Error;

use shuffle_backup_converter::{Args, CliArgsParsingError};
use shuffle_backup_converter::convert_backup_file;
use shuffle_backup_converter::use_user_interface;

fn main() -> Result<(), Box<dyn Error>> {
    let args = parse_args()?;
    match args {
        Args { inputfile: Some(input_file), outputfile: Some(output_file) } => convert_backup_file(&input_file, &output_file),
        Args { inputfile: Some(input_file), outputfile: None } => convert_backup_file(&input_file, &input_file.replace("bak", "csv")),
        _ => use_user_interface(),
    }
}

fn parse_args() -> Result<Args, Box<CliArgsParsingError>> {
    let args: Vec<String> = env::args().collect();

    if 1 == args.len() {
        return Ok(Args::default());
    }
    if 3 < args.len() {
        return Err(Box::new(CliArgsParsingError::new("Too many arguments. Usage: [optional input file] [optional output file]\noutputfile defaults to the input file's name with a changed extension, i.e. `example.bak` gets converted to `example.csv`.")));
    }

    let filename = &args[1];
    let outputfile = if args.len() == 3 { Some(args[2].clone()) } else { None };

    let args = Args {
        inputfile: Some(filename.clone()),
        outputfile,
    };

    Ok(args)
}


