//! fooo
//! df
use std::fs::File;
use std::io;
use std::io::Read;

use prost::bytes::{BufMut, BytesMut};
use prost::Message;
use crate::proto::Catalogue;

/// does stuff
pub fn read_backup_file(filename: &String) -> Result<Catalogue, io::Error> {
    let mut file = File::open(filename)?;

    let mut buf = Vec::new();
    file.read_to_end(&mut buf)?;

    let mut bytes_buf = BytesMut::new();
    for b in buf {
        bytes_buf.put_u8(b);
    }

    let decoded_catalogue = Catalogue::decode(bytes_buf)?;

    Ok(decoded_catalogue)
}
