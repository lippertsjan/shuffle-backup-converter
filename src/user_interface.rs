use crate::convert_backup_file;

use druid::commands;
use druid::AppDelegate;
use druid::AppLauncher;
use druid::Command;
use druid::Data;
use druid::DelegateCtx;
use druid::Env;
use druid::FileDialogOptions;
use druid::FileSpec;
use druid::Handled;
use druid::Lens;
use druid::Size;
use druid::Target;
use druid::Widget;
use druid::WindowDesc;
use druid::Selector;
use druid::widget::Align;
use druid::widget::Button;
use druid::widget::Flex;
use druid::widget::Label;

use std::error::Error;

struct Delegate;

#[derive(Clone, Data, Lens)]
struct State {
    input_file: String,
    message: String
}

const MSG_PICK: &str = "Pick a .bak file.";
const MSG_CONVERT: &str = "Click Convert";
const MSG_SUCCESS: &str = "Conversion successful.";

impl State {
    fn default() -> State {
        State {input_file: "".to_owned(), message: MSG_PICK.to_owned()}
    }
}
pub fn use_user_interface() -> Result<(), Box<dyn Error>> {
    let size = Size { width: 240.0, height: 120.0 };

    let main_window = WindowDesc::new(ui_builder)
        .title("Shuffle Backup Converter")
        .with_min_size(size)
        .window_size(size);

    let data = State::default();

    AppLauncher::with_window(main_window)
        .delegate(Delegate)
        .use_simple_logger()
        .launch(data)
        .expect("launch failed");

    Ok(())
}


fn ui_builder() -> impl Widget<State> {
    let bak = FileSpec::new("Shuffle Backup", &["bak"]);
    // The options can also be generated at runtime,
    // so to show that off we create a String for the default save name.

    let open_dialog_options = FileDialogOptions::new()
        .allowed_types(vec![bak])
        .default_type(bak)
        .name_label("Shuffle Backup")
        .title("Pick a Shuffle backup file")
        .button_text("Pick");

    let input: Label<State> = Label::new(|data: &State, _: &Env| format!("{}", data.input_file));
    let message_label: Label<State> = Label::dynamic(|data: &State, _: &Env| format!("{}", data.message));

    let open = Button::new("Pick").on_click(move |ctx, _, _| {
        ctx.submit_command(Command::new(
            druid::commands::SHOW_OPEN_PANEL,
            open_dialog_options.clone(),
            Target::Auto,
        ))
    });
    let convert = Button::new("Convert").on_click(move |ctx, _, _| {
        ctx.submit_command(Command::new(
            CONVERT_COMMAND,
            (),
            Target::Auto,
        ))
    });

    let mut button_row = Flex::row();
    button_row.add_child(open);
    button_row.add_spacer(8.0);
    button_row.add_child(convert);


    let mut col = Flex::column();
    col.add_child(input);
    col.add_spacer(8.0);
    col.add_child(button_row);
    col.add_spacer(8.0);
    col.add_child(message_label);
    Align::centered(col)
}

const CONVERT_COMMAND: Selector = Selector::new("command.convert");

impl AppDelegate<State> for Delegate {
    fn command(
        &mut self,
        _ctx: &mut DelegateCtx,
        _target: Target,
        cmd: &Command,
        data: &mut State,
        _env: &Env,
    ) -> Handled {
        if let Some(file_info) = cmd.get(commands::OPEN_FILE) {
            let mut picked_path = String::new();
            picked_path.push_str(file_info.path().to_str().unwrap());

            data.input_file = picked_path;
            data.message = MSG_CONVERT.to_owned();

            return Handled::Yes;
        }

        if let Some(_) = cmd.get(CONVERT_COMMAND) {
            let input_file = data.input_file.clone();
            if input_file.is_empty() {
                return Handled::Yes;
            }

            let output_file = input_file.replace(".bak", ".csv");

            println!("Converting {} to {}...", input_file, output_file);

            match convert_backup_file(&input_file, &output_file) {
                Ok(_) => data.message = MSG_SUCCESS.to_owned(),
                Err(e) => data.message = format!("{}", e)
            }


            return Handled::Yes;
        }

        Handled::No
    }
}
